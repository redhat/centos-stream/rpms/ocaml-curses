#!/bin/bash -
set -e
set -x

# Compile trivial curses program.
echo 'open Curses;; ignore (initscr ()); endwin ()' > cursestest.ml
ocamlfind ocamlopt -package curses cursestest.ml -linkpkg -o cursestest
./cursestest
